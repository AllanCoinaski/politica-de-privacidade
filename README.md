Esta página é usada para informar os visitantes sobre nossas políticas de coleta, uso e divulgação de Informações Pessoais, caso alguém decida usar nosso Serviço. Se você optar por usar nosso serviço, você concorda em coletar e usar informações relacionadas a esta política. As informações pessoais coletadas são utilizadas para fornecer e melhorar o serviço. Os termos utilizados nesta Política de Privacidade têm os mesmos significados dos nossos Termos e Condições, e são acessíveis a ZITA, a menos que definido de outra forma nesta Política de Privacidade.

### Informações de coleta e uso ###
Nós apenas coletamos suas informações pessoais quando você encomendar bens ou serviços no aplicativo. Coletamos essas informações de modo a permitir que você faça pedidos, e as informações coletadas são: Nome, CPF, e-mail, endereço, número de telefone ou informações de pagamento.
Informações que coletamos sobre você são usadas para processar seu pedido e para gerenciar sua conta. Nós também podemos usar suas informações para enviar e-mail sobre outros produtos ou serviços que acreditamos ser de seu interesse.
No processamento de seu pedido, podemos enviar suas informações para referência de crédito e agências de prevenção de fraude, terceiros (empresas de serviços e empresas de entrega), serviços do google Play, loja de aplicativos e provedores de serviço.
Podemos empregar empresas terceirizadas e indivíduos pelos seguintes motivos: • Para facilitar nosso serviço; 

* Para fornecer o Serviço em nosso nome; 
* Realizar serviços relacionados ao serviço; ou 
* Para nos ajudar a analisar como nosso Serviço é usado. 

Queremos informar aos usuários deste serviço que esses terceiros têm acesso às suas informações pessoais. O motivo é realizar as tarefas atribuídas a eles em nosso nome. No entanto, eles são obrigados a não divulgar ou usar as informações para qualquer outra finalidade. Como o serviço que você está contratando é terceirizado, não nos responsabilizamos pelo serviço prestado pelo profissional.

### Segurança ###
Valorize sua confiança em nos fornecer suas informações pessoais, portanto, estamos nos empenhando para usar meios comercialmente aceitáveis de protegê-las. Lembre-se de que nenhum método de transmissão pela internet ou método de armazenamento eletrônico é 100% seguro e confiável, e não é possível garantir sua segurança absoluta.

### Acesso às suas informações ###
A qualquer momento, você pode solicitar uma cópia das informações que temos sobre você. Basta contactar-nos em contato@appzita.com. Vamos pedir-lhe o pagamento de uma pequena taxa, a fim de processar o seu pedido.

### Links para outros sites ###
Este serviço pode conter links para outros sites. Se você clicar em um link de terceiros, será direcionado a esse site. Observe que esses sites externos não são operados por nós. Portanto, recomendamos enfaticamente que você analise a Política de Privacidade desses sites. Não temos controle e não assumimos responsabilidade pelo conteúdo, políticas de privacidade ou práticas de sites ou serviços de terceiros.

### Privacidade das crianças ###
Este Serviço não se dirige a ninguém com idade inferior a 13 anos. Não recolhemos intencionalmente informações de identificação pessoal de crianças com menos de 13 anos. No caso de descobrirmos que uma criança com menos de 13 anos nos forneceu informações pessoais, as eliminamos imediatamente dos nossos servidores. Se você é um pai ou responsável e sabe que seu filho nos forneceu informações pessoais, entre em contato para que possamos tomar as medidas necessárias.

### Mudanças nesta Política de Privacidade ###
Podemos atualizar nossa Política de Privacidade de tempos em tempos. Portanto, é recomendável revisar esta página periodicamente para verificar se há alterações. Iremos notificá-lo de quaisquer alterações, publicando a nova Política de Privacidade nesta página. Esta política entra em vigor em 26 de agosto de 2021

### Contate-Nos ###
Se você tiver dúvidas ou sugestões sobre nossa Política de Privacidade, não hesite em entrar em contato conosco pelo e-mail contato@appzita.com.